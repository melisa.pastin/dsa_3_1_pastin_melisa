#include <stdio.h>
#include <stdbool.h>

#define N 4

int board[N][N];

int moves[8][2] = {{-2, -1}, {-1, -2}, {1, -2}, {2, -1}, {2, 1}, {1, 2}, {-1, 2}, {-2, 1}};

bool isSafe(int x, int y) 
{
    return (x >= 0 && x < N && y >= 0 && y < N && board[x][y] == -1);
}

void printBoard() 
{
    for (int i = 0; i < N; i++) 
    {
        for (int j = 0; j < N; j++) 
        {
            printf("%d ", board[i][j]);
        }
        printf("\n");
    }
}

bool knightTour(int x, int y, int moveCount) 
{
    if (moveCount == N * N) 
    {
        return true; 
    }

    for (int i = 0; i < 8; i++) 
    {
        int nextX = x + moves[i][0];
        int nextY = y + moves[i][1];

        if (isSafe(nextX, nextY)) 
        {
            board[nextX][nextY] = moveCount + 1;

            if (knightTour(nextX, nextY, moveCount + 1)) 
            {
                return true;
            }

            board[nextX][nextY] = -1;
        }
    }

    return false;
}

int main() 
{

    for (int i = 0; i < N; i++) 
    {
        for (int j = 0; j < N; j++) 
        {
            board[i][j] = -1;
        }
    }

    int startX = 0, startY = 0;
    board[startX][startY] = 0;

    if (knightTour(startX, startY, 0))
    {
        printf("solution:\n");
        printBoard();
    } 
    else 
    {
        printf("no solution\n");
    }

    return 0;
}
