#include <stdio.h>

int x[10], n;

void print(int k)
{
    for(int i = 1 ; i <= k  ; i++)
    {
       printf("%d ", x[i]);
    }
    printf("\n");
}

int valid(int k)
{
    for(int i = 1 ; i < k ; i++)
    {
        if(x[i] == x[k])
        {
            return 0;
        }
    }
    return 1;
}

void back()
{
    int k = 1;
    x[1] = 0;
    while(k > 0)
    {
        int find = 0;
        do{
            x[k]++;
            if(x[k] <= n && valid(k) == 1)
            {
                find = 1;
            }
        }
        while(find == 0 && x[k] <= n);
        if(find == 0)
        {
            k--;
        }
        else
        {
            if(k < n)
            {
                k++;
                x[k] = 0;
            }
            else
            {
                print(k);
            }
        }

    }
}

int main()
{
    n = 3;
    back();
    return 0;
}