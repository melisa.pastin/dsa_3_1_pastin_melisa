#include <stdio.h>

int x[10], n;

void print()
{
    for(int i = 1; i <= n; i++)
    {
        printf("%d ", x[i]);
    }
    printf("\n");
}

int valid(int k)
{
    for(int i = 1; i < k; i++)
    {
        if(x[k] == x[i])
        {
            return 0;
        }
    }
    return 1;
}

int solution(int k)
{
    if(k == n)
    {
        return 1;
    }
    else
    {
        return 0;
    }
}

void back(int k)
{
    for(int i = 1; i <= n ; i++)
    {
        x[k] = i;
        if(valid(k) == 1)
        {
            if(solution(k) == 1)
            {
                print();
            }
            else
            {
                back(k + 1);
            }
        }
    }
}

int main()
{
    n = 3;
    back(1);
    return 0;
}