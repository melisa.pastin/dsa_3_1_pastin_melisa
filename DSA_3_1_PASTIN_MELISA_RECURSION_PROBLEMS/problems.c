#include <stdio.h>

//p1
int recursively(int n)
{
    if(n == 0)
    {
        return 1;
    }

    return n * recursively(n - 1);
}

int iteratively(int n)
{
    int factorial = 1;
    for (int i = 1; i <= n; i++)
    {
        factorial = factorial * i;
    }
    return factorial;
}

//p2
int fibonacciR(int n) 
{
    if (n == 0) 
    {
        return n;
    } 

    if (n == 1) 
    {
       return n;
    }

    return fibonacciR(n - 1) + fibonacciR(n - 2);
}

int fibonacciI(int n)
{
    int first = 0;
    int second = 1;
    int result;

    for (int i = 0; i < n; i++)
    {
        if (i == 0)
        {
            result = 0;
        }
        else
        {
            if (i == 1)
            {
                result = 1;
            }
            else
            {
                result = first + second;
                first = second;
                second = result;
            }
        }
    }

    return result + 1;
}

//p3
int gcd(int a, int b)
{
    if(a == b)
    {
        return a;
    }
    else
    {
        if(a > b)
        {
            return gcd(a - b, b);
        }
    }

    return gcd(a, b - a);
}

//p4
int MC(int n)
{
    if(n > 100)
    {
        return n - 10;
    }

    return MC(MC(n + 11));
}

int main()
{
    printf("%d", recursively(4));
    printf("\n");
    printf("%d", iteratively(4));
    printf("\n");

    printf("%d", fibonacciR(4));
    printf("\n");
    printf("%d", fibonacciI(4));
    printf("\n");

    printf("%d", gcd(12, 6));
    printf("\n");

    printf("%d", MC(90));
}