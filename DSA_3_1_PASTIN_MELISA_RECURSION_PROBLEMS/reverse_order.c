#include <stdio.h>

int reverse_string()
{
    char s;
    s = getc(stdin);
    if (s == '0')
    {
        return 0;
    }

    reverse_string();
    putchar(s);
    return 0;
}
 
int main()
{
    reverse_string();
    return 0;
}