#include <stdio.h>

int sumOfDigits(int n)
{
    if(n/10 == 0)
    {
        return n;
    }

    return n%10 + sumOfDigits(n/10);
}

int main()
{
    int n;
    scanf("%d", &n);
    printf("the sum of all digits: %d", sumOfDigits(n));
    return 0;
}