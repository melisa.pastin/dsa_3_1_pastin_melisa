#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

struct wordsFromFile
{
    char word[50];
    int occ;
    double freq;
} v[1000];

int readFromFile()
{
    char buf[4096];
    char *s;
    int i = 0;

    FILE *f = fopen("words - sorted.csv", "r");
    if (f == NULL)
    {
        perror("fopen");
        return 0;
    }

    while (fgets(buf, 4096, f))
    {
        s = strtok(buf, ",");
        strcpy(v[i].word, s);

        s = strtok(NULL, ",");
        v[i].occ = atoi(s);

        s = strtok(NULL, ",");
        v[i].freq = atof(s);

        i++;
    }

    fclose(f);
    return i;
}

int binarySearch(const char *targetWord, const struct wordsFromFile *v, int i, FILE *outputFile)
{
    int left = 0, right = i - 1;

    while (left <= right)
    {
        int mid = left + (right - left) / 2;

        if (strcmp(v[mid].word, targetWord) == 0)
        {
            fprintf(outputFile, "word found: %s\n", v[mid].word);
            return mid; 
        }
        else
        {
            if (strcmp(v[mid].word, targetWord) < 0)
            {
                left = mid + 1; 
            }
            else
            {
                right = mid - 1; 
            }
        }
    }

    fprintf(outputFile, "word not found\n");
    return 0; 
}

double minFrequency(const char *targetWord, const struct wordsFromFile *v, int i)
{
    double minFreq = 1.0;

    for (int j = 0; j < i; j++)
    {
        if (strcmp(v[j].word, targetWord) == 0)
        {
            if (v[j].freq < minFreq)
            {
                minFreq = v[j].freq;
            }
        }
    }

    return minFreq;
}

int maxOccurrence(const char *targetWord, const struct wordsFromFile *v, int i)
{
    int maxOcc = -1;

    for (int j = 0; j < i; j++)
    {
        if (strcmp(v[j].word, targetWord) == 0)
        {
            if (v[j].occ > maxOcc)
            {
                maxOcc = v[j].occ;
            }
        }
    }

    return maxOcc;
}

void occurrences(const struct wordsFromFile *v, int i, int w[], int x[])
{
    int k = 0;

    for (int j = 0; j < i; j++)
    {
        if (v[j].occ > 0)
        {
            w[k] = v[j].occ;
            x[k] = v[j].occ;
            k++;
        }
    }
}

void bubbleSort(int w[], int k, FILE *outputFile)
{
    clock_t start_time = clock();    //the clock time when the sorting begins

    for (int i = 0; i < k - 1; i++) 
    {
        for(int j = i + 1; j < k; j++)
        {
            if (w[i] < w[j]) 
            {
                int aux = w[i];
                w[i] = w[j];
                w[j] = aux;
            }
        }
    }

    clock_t end_time = clock();   //the clock time when the sorting ends
    double execution_time = ((double)(end_time - start_time)) / CLOCKS_PER_SEC * 1000; //time in milliseconds

    fprintf(outputFile, "bubble sort: %lf ms\n", execution_time);
}

void quickSort(int x[], int low, int high)
{
    if (low < high)
    {
        int pivot = x[high];
        int i = low - 1;

        for (int j = low; j <= high - 1; j++)
        {
            if (x[j] >= pivot)
            {
                i++;
                int aux = x[i];
                x[i] = x[j];
                x[j] = aux;
            }
        }

        // the index of the right position of the pivot
        int pivotPosition = i + 1;

        // put the pivot in its correct position
        int aux = x[pivotPosition];
        x[pivotPosition] = x[high];
        x[high] = aux;

        quickSort(x, low, pivotPosition - 1);
        quickSort(x, pivotPosition + 1, high);
    }
}

void wrapper(int x[], int k, FILE *outputFile)
{
    clock_t start_time = clock();
    quickSort(x, 0, k - 1);
    clock_t end_time = clock();
    double execution_time = ((double)(end_time - start_time)) / CLOCKS_PER_SEC * 1000;
    fprintf(outputFile, "quicksort: %lf ms\n", execution_time);
}

void printArrays(int w[], int x[], int k, FILE *outputFile)
{
    fprintf(outputFile, "\n");

    bubbleSort(w, k, outputFile);
    for (int i = 0; i < k; i++)
    {
        if (w[i] != w[i + 1])
        {
            fprintf(outputFile, "%d ", w[i]);
        }
    }
    fprintf(outputFile, "\n");

    wrapper(x, k, outputFile);
    for (int i = 0; i < k; i++)
    {
        if (x[i] != x[i + 1])
        {
            fprintf(outputFile, "%d ", x[i]);
        }
    }
    fprintf(outputFile, "\n");
}

int main()
{
    int i = 0;

    if ((i = readFromFile()) == 0)
    {
        exit(0);
    }

    FILE *f = fopen("output.txt", "w");
    if (f == NULL)
    {
        perror("fopen");
        return 0;
    }

    binarySearch("along", v, i, f);

    double minFreq = minFrequency("along", v, i);
    if (minFreq != 1.0)
    {
        fprintf(f, "minimum frequency for 'along': %lf\n", minFreq);
    }
    else
    {
        fprintf(f, "word not found\n");
    }

    int maxOcc = maxOccurrence("along", v, i);
    if (maxOcc != -1)
    {
        fprintf(f, "maximum occurrence for 'along': %d\n", maxOcc);
    }
    else
    {
        fprintf(f, "word not found\n");
    }

    int w[1000];
    int x[1000];

    occurrences(v, i, w, x);
    printArrays(w, x, i, f);

    fclose(f);

    return 0;
}
