#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct word_line{
	char* word;
	int occ;
	double freq;
}v[1000];

int main()
{
	char buf[4096];
	char* s;
	int i = 0;

	FILE *f = fopen("words.csv" , "r");
	if (f == NULL)
	{
		perror("fopen");
		exit(0);
	}
	//printf("file opened\n");

	while (fgets(buf, 4096, f))
	{
		s = strtok(buf, ",");
		strcpy(v[i].word, s);
		printf("%s ", v[i].word);
		s = strtok(NULL, ",");
		v[i].occ = atoi(s);
		printf("%d ", v[i].occ);
 		s = strtok(NULL, ",");
		v[i].freq = atof(s);
		printf("%ls\n", v[i].freq);
		i++;
	}

	fclose(f);
	return 0;
}