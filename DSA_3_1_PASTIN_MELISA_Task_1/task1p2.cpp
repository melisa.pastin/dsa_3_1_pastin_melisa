#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct wordsFromFile
{
	char word[50];
	int occ;
	double freq;
} v[1000];

int readFromFile()
{
	char buf[4096];
	char *s;
	int i = 0;

	FILE *f = fopen("words - sorted.csv", "r");
	if (f == NULL)
	{
		perror("fopen");
		return 0;
	}

	while (fgets(buf, 4096, f))
	{
		s = strtok(buf, ",");
		strcpy(v[i].word, s);

		s = strtok(NULL, ",");
		v[i].occ = atoi(s);

		s = strtok(NULL, ",");
		v[i].freq = atof(s);

		i++;
	}

	fclose(f);
	return i;
}

int main()
{
	int i = 0;

	if (!readFromFile())
	{
		exit(0);
	}
	else
	{
		i = readFromFile();
	}

	float minFreq = 10.0;
	int maxOcc = -1;
	char givenWord[50] = "along";

	int left, right, m, found;
	left = 0; right = i - 1; found = 0;
	while ((left <= right) && (!found))
	{
		m = (left + right) / 2;
		if (strcmp(v[m].word, givenWord) == 0)
		{
			found = 1;
			if (minFreq > v[m].freq)
			{
				minFreq = v[m].freq;
			}


			if (maxOcc < v[m].occ)
			{
				maxOcc = v[m].occ;
			}
		}
		else
		{
			if (strcmp(v[m].word, givenWord) < 0)
			{
				left = m + 1;
			}
			else
			{
				right = m - 1;
			}
		}
	}

	printf("%lf %d\n", minFreq, maxOcc);

	return 0;
}