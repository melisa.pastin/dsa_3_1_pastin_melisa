#include <stdio.h>
#include <stdlib.h>

typedef struct node{
    int val;
    struct node *next;
}node;
node *head, *tail, *crt;

void init_list(node **head, node **tail, node **crt)
{
    *head = *tail = *crt;
}

node* create_node(int val)
{
    node *p;
    p = (node *)malloc(sizeof(node));

    if(p == NULL)
    {
        printf("error at memory allocation\n");
        exit(1);
    }

    p -> val = val;
    p -> next = NULL;

    return p;
}

void print_list(node *p)
{
    while(head != NULL)
    {
        printf("%d ", p -> val);
        p = p -> next;
    }
}

void delete(node **head)
{
    node *crt = *head;
    node *next;

    while(crt != NULL)
    {
        next = crt -> next;
        free(crt);
        crt = next;
    }

    *head = NULL;
}

int main()
{
    struct node *head;
    create_node(1);
    create_node(5);
    create_node(6);
    create_node(9);
    print_list(&head);
    delete(&head);

    return 0;
}




