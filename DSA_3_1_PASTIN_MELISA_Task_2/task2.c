#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct node {
    int id;
    int address;
    char *description;
    int size;
    struct node *next;
} node;

node *head, *tail;

void init_list() 
{
    head = tail = NULL;
}

node *create_node(int id, int address, char *description, int size) 
{
    node *p = (node *)malloc(sizeof(node));
    if (p == NULL) 
    {
        printf("error at memory allocation\n");
        exit(1);
    }

    p->description = (char *)malloc(strlen(description) + 1);
    if (p->description == NULL) 
    {
        printf("error at memory allocation\n");
        exit(1);
    }
    strcpy(p->description, description);

    p->id = id;
    p->address = address;
    p->size = size;
    p->next = NULL;

    return p;
}

void add_node(node *node_to_add) 
{
    if (tail == NULL) 
    {
        head = node_to_add;
    } 
    else 
    {
        tail->next = node_to_add;
    }

    tail = node_to_add;
    node_to_add->next = NULL;
}

void print_list() 
{
    node *p = head;
    if (p == NULL) 
    {
        printf("the list is empty\n");
    } 
    else 
    {
        while (p) 
        {
            printf("ID: 0x%X, Address: 0x%X, Description: %s, Size: %d\n", p->id, p->address, p->description, p->size);
            p = p->next;
        }
    }
}

void delete_list() 
{
    node *p;
    while (head != NULL) 
    {
        p = head;
        head = head->next;
        free(p->description);
        free(p);
    }
    tail = NULL;
}

void OptimizeResources(int *totalOptimizedMemory) 
{
    node *p = head;

    while (p != NULL && p->next != NULL) 
    {
        if (p->address + p->size != p->next->address) 
        {
            int optimizedMemory = p->address + p->size - p->next->address;
            p->next->address = p->address + p->size;
            *totalOptimizedMemory = *totalOptimizedMemory + optimizedMemory;
        }

        p = p->next;
    }
}

void CheckIntegrity() 
{
    node *p = head;
    int totalOptimizedMemory = 0;

    while (p != NULL && p->next != NULL) 
    {
        if (p->address >= p->next->address) 
        {
            printf("memory addresses are NOT in ascending order\n");
            OptimizeResources(&totalOptimizedMemory);
            printf("total optimized memory: %d B\n", totalOptimizedMemory);
            return;
        }

        int expectedAddress = p->address + p->size;

        if (expectedAddress != p->next->address) 
        {
            printf("wrong address\n");
            OptimizeResources(&totalOptimizedMemory);
            printf("total optimized memory: %d B\n", totalOptimizedMemory);
            return;
        }

        p = p->next;
    }
}

int main() 
{
    char buf[4096];
    int id;
    int address;
    char *description;
    int size;

    FILE *fp = fopen("input.csv", "r");
    if (fp == NULL) 
    {
        perror("fopen");
        return 0;
    }

    init_list();

    while (fgets(buf, sizeof(buf), fp) != NULL) 
    {
        char *token = strtok(buf, ",");
        id = strtol(token, NULL, 16);
        token = strtok(NULL, ",");
        address = strtol(token, NULL, 16);
        token = strtok(NULL, ",");
        description = strdup(token);
        token = strtok(NULL, ",");
        size = atoi(token);

        add_node(create_node(id, address, description, size));
    }

    fclose(fp);

    printf("initial list:\n");
    print_list();
    CheckIntegrity();
    printf("optimized list:\n");
    print_list();  
    delete_list();

    return 0;
}
